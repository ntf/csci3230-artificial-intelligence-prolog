
%move (coordinate)

%move getter
get_move_x(move(X,_),X). 
get_move_y(move(_,Y),Y).
%move setter
set_move_x(X,move(_,Y),move(X,Y)).
set_move_y(Y,move(X,_),move(X,Y)).
replace_move_x(OldX,NewX,pos(OldX,   Y),pos(NewX,   Y)).
replace_move_y(OldY,NewY,pos(   X,OldY),pos(   X,NewY)).

%size of the board with move(X,Y) representation
sizeof_board(Board,Size):-
	length(Board,X),
	Board = [Row|_],
	%nth1(X,Board,Row),
	length(Row,Y),
	MaxX is X - 1,
	MaxY is Y - 1,
	Size = move(MaxX,MaxY).

%update board value
update_board(Pos,Value,Board,UpdatedBoard):-
	get_move_x(Pos,X),
	get_move_y(Pos,Y),
	%replace_nth0(X,E1,E2,Board,UpdatedBoard),
	%replace_nth0(Y,E1,_,Value,E2).
	X1 is X + 1,
	Y1 is Y + 1,
	record:replace_nth(X1,OldList,OldSubList,Board),
	record:replace_nth(Y1,OldSubList,Value,NewSubList),
	record:replace_nth(X1,OldList,NewSubList,UpdatedBoard).

%count occurrences of a value on a board
occurrences(_,[],0).
occurrences(X,[X|Y],N):- occurrences(X,Y,W), N is W + 1.
occurrences(Z,[X|Y],N):- occurrences(Z,Y,N), X\= Z. % pass to next element

count_board(Value,Board,Count):-
	maplist(occurrences(Value),Board,Counts),
	%write(Counts),
	sum_list(Counts,Count).


%get value by move
get_value_by_move(Pos,Board,Value):-
	get_move_x(Pos,X),
	get_move_y(Pos,Y),
	nth0(X, Board, E),
	nth0(Y,E,Value).
get_value_by_pos(X,Y,Board,Value):-
	X >= 0,
	Y >= 0,
	nth0(X, Board, E, _), nth0(Y,E,Value,_).
	
	
%Function 1
%life_and_death([],_,[]).
life_and_death(Board,LastMove,NewBoard):- 
	Pos = move(0,0),
	sizeof_board(Board,Size),
	maplist_x(ld_objective,Board,Pos,Size,LastMove, Board , NewBoard), !.

	

maplist_x(Goal,Board,Pos,Size,LastMove, [           ], [           ]).
maplist_x(Goal,Board,Pos,Size,LastMove, [Elem1|Tail1], [Elem2|Tail2]) :-
	maplist_y( Goal , Board , Pos ,Size,LastMove, Elem1 , Elem2),
	get_move_x(Pos,X),
	NextX is X+1,
	set_move_x(NextX,Pos,NextPos),
	maplist_x(Goal,Board,NextPos,Size,LastMove, Tail1, Tail2).

maplist_y(Goal,Board,Pos,Size,LastMove, [           ], [           ]).
maplist_y(Goal,Board,Pos,Size,LastMove, [Elem1|Tail1], [Elem2|Tail2]) :-
	ld_objective(Board,Pos,Size,LastMove, Elem1, Elem2),
	get_move_y(Pos,Y),
	NextY is Y+1,
	set_move_y(NextY,Pos,NextPos),
	maplist_y(Goal,Board,NextPos,Size,LastMove, Tail1, Tail2).

	
% b = black , w = white  , m = vacant + can put , s = vacant + can't put
% bL : black + hasL , wL : white + hasL , bD : black + noL , wD : white + noL , aL : vacant

ld_objective(Board,Pos,Size,LastMove,Input,Output):- Input = m , Output = aL.
ld_objective(Board,Pos,Size,LastMove,Input,Output):- Input = s , Output = aL.

ld_objective(Board,Pos,Size,LastMove,Input,Output):- Input = b, 
	(
	LastMove = Pos;
	ld_getL(Input,Pos,Size,Board,LastMove,L)
	),
	%write('bL'),
	Output = bL.
	
ld_objective(Board,Pos,Size,LastMove,Input,Output):- Input = b,
	% write('bD'),
	Output = bD.
ld_objective(Board,Pos,Size,LastMove,Input,Output):- Input = w, 
	(
	LastMove = Pos;
	ld_getL(Input,Pos,Size,Board,LastMove,L)
	),
	%write('wL'),
	Output = wL.
ld_objective(Board,Pos,Size,LastMove,Input,Output):- Input = w,
	%write('wD'),
	Output = wD.


%/6 to /7
ld_getL(Input,Pos,Size,Board,LastMove,Liberty):-
	Visited = [],
%	nl,
%	write('start Processing '),
%	write(Pos),
%	write(' '),
%	write(Input),
%	nl,
	ld_getL(Input,Pos,Size,Board,LastMove,Liberty,Visited).

%/7 start	
ld_getL(Input,Pos,Size,Board,LastMove,Liberty,Visited):-
	
	get_value_by_move(Pos,Board,ThisValue),
	
%	write('=>'),
%	write(Pos),
%	write(ThisValue),
%	nl,
	
	(
		(
		ThisValue = s;
		ThisValue = m
		),
		Liberty = Pos
	).

ld_getL(Input,Pos,Size,Board,LastMove,Liberty,Visited):-
	get_value_by_move(Pos,Board,ThisValue),
	(
		ThisValue \= Input,
		fail
	).

%left
ld_getL(Input,Pos,Size,Board,LastMove,Liberty,Visited):-

	get_move_y(Pos,Y),
	LeftY is Y -1,
	LeftY >= 0,
	get_value_by_move(Pos,Board,ThisValue),
	ThisValue = Input,
%	write(' left '),
	set_move_y(LeftY,Pos,Temp),
	(\+ member(Temp,Visited)),
	append(Visited,[Temp],NextVisited),
	ld_getL(Input,Temp,Size,Board,LastMove,Liberty,NextVisited).

%top
ld_getL(Input,Pos,Size,Board,LastMove,Liberty,Visited):-
	
	get_move_x(Pos,X),
	
	TopX is X - 1,
	TopX >= 0,
	get_value_by_move(Pos,Board,ThisValue),
	ThisValue = Input,
%	write(' top '),
	set_move_x(TopX,Pos,Temp),
	(\+ member(Temp,Visited)),
	append(Visited,[Temp],NextVisited),
	ld_getL(Input,Temp,Size,Board,LastMove,Liberty,NextVisited).
%bottom	
ld_getL(Input,Pos,Size,Board,LastMove,Liberty,Visited):-
	
	get_move_x(Pos,X),
	get_move_x(Size,MaxX),
	BottomX is X + 1,
	BottomX =< MaxX,
	
	get_value_by_move(Pos,Board,ThisValue),
	ThisValue = Input,
%	write(' bottom '),
	set_move_x(BottomX,Pos,Temp),
	(\+ member(Temp,Visited)),
	append(Visited,[Temp],NextVisited),
	ld_getL(Input,Temp,Size,Board,LastMove,Liberty,NextVisited).
%right
ld_getL(Input,Pos,Size,Board,LastMove,Liberty,Visited):-
	
	get_move_y(Pos,Y),
	get_move_y(Size,MaxY),
	RightY is Y + 1,
	RightY =< MaxY,	
	
	get_value_by_move(Pos,Board,ThisValue),
	ThisValue = Input,
%	write(' right '),
	set_move_y(RightY,Pos,Temp),
	(\+ member(Temp,Visited)),
	append(Visited,[Temp],NextVisited),
	ld_getL(Input,Temp,Size,Board,LastMove,Liberty,NextVisited).	
	
%Function 2
find_good_move(WhoMoveFirst,Depth,Board,BestMove,ScoreOfBestMove):-
	score(Board,_,WhoMoveFirst,Depth,BestMove,ScoreOfBestMove).
	%BestMove = move(4,3),
	%ScoreOfBestMove = 0.
	
	
% P either b or w
score_move(P, BoardOrigin, Board,Move,Score):-

	%replace position Move with P Value on Board and store to Board After
	update_board(Move,P,Board,BoardTemp), 
	%write(BoardTemp),
	%execute Move by Player P assume no self-capture
	life_and_death(BoardTemp,Move,BoardAfter),
	%write(BoardAfter),
	(
		(P = b,
		count_board(b,BoardOrigin,C0),
		count_board(bL,BoardAfter,C1),
		count_board(w,BoardOrigin,C2),
		count_board(wL,BoardAfter,C3)
		);
		(P = w,
		count_board(w,BoardOrigin,C0),
		count_board(wL,BoardAfter,C1),
		count_board(b,BoardOrigin,C2),
		count_board(bL,BoardAfter,C3)
		)
	),
	Score is (C1 - C0) + (C2 - C3),
	write('END SCOREMOVE score is '),write(Score),nl.
	
	
score(BoardOrigin,Board,P,Depth,BestMove,Score):-
	Depth < 0,fail.
score(BoardOrigin,Board,P,Depth,BestMove,Score):-
	Depth == 0,
	BestMove = move(-1,-1),
	Score = 2147483647.
score(BoardOrigin,Board,P,Depth,BestMove,Score):-
	Depth == 1,
	Pos = move(0,0),
	sizeof_board(BoardOrigin,Size),
	Board = BoardOrigin,	
	maplist2_x(score_objective,BoardOrigin,Board,Pos,Size,P, Board , ScoreList),
	max_score(ScoreList,BestResult),
	get_pos_from_result(BestResult,BestMove),
	get_score_from_result(BestResult,Score). 
score(BoardOrigin,Board,P,Depth,BestMove,Score):-
	Depth > 1.
	% minmax start here 
	
	

	
%max_board_x(Board,X,Y,Score):-
%	get_value_by_pos(
	
%score move maplist 
maplist2_x(Goal,BoardOrigin,Board,Pos,Size,LastMove, [           ], [           ]).
maplist2_x(Goal,BoardOrigin,Board,Pos,Size,LastMove, [Elem1|Tail1], ScoreBoard) :-
	maplist2_y( Goal ,BoardOrigin, Board , Pos ,Size,LastMove, Elem1 ,Elem2),

	get_move_x(Pos,X),
	%(X >= MaxX;
	NextX is X+1,
	set_move_x(NextX,Pos,NextPos),
	append(Elem2,Tail2,ScoreBoard),
	%write(ScoreResult),
	maplist2_x(Goal,BoardOrigin,Board,NextPos,Size,LastMove, Tail1, Tail2)
	%)
	.

maplist2_y(Goal,BoardOrigin,Board,Pos,Size,LastMove, [           ], [           ]).
maplist2_y(Goal,BoardOrigin,Board,Pos,Size,LastMove, [Elem1|Tail1], [Elem2|Tail2]) :-
	score_objective(BoardOrigin,Board,Pos,Size,LastMove, Elem1, Elem2),
	get_move_y(Pos,Y),
	%(Y >= MaxY;
	NextY is Y+1,
	set_move_y(NextY,Pos,NextPos),
	maplist2_y(Goal,BoardOrigin,Board,NextPos,Size,LastMove, Tail1, Tail2)
	%)
	.


%score objective Input : m/s/b/w Output : Score of that move

get_pos_from_result(result(move(X,Y),_),move(X,Y)). 
get_score_from_result(result(_,S),S). 

score_objective(BoardOrigin,Board,Pos,Size,Player,Input,Output):-
	%m: a vacant space that the players can place a stone.
	Input = m ,
	score_move(Player, BoardOrigin, Board,Pos,Out),
	Output = result(Pos,Out).
	%get_score_from_result(Output,Test99),
	%write(Test99).
	%Output = Score.
score_objective(BoardOrigin,Board,Pos,Size,Player,Input,Output):-
	Output = result(Pos,-255).
%score_objective(BoardOrigin,Board,Pos,Size,Player,Input,Output):-
	%d s: a vacant space that both players cannot place a stone.
%	Input = s.
	%fail.


%Helpers
plus(A, B, C) :- C is A + B.
sum_list([], 0).
sum_list([H|T], Sum) :-
   sum_list(T, Rest),
   Sum is H + Rest.
   
max_score([H],H).
max_score([H1,H2|T],X):-
	get_score_from_result(H1,Score1),
	get_score_from_result(H2,Score2),
	Score1 > Score2,
	max_score([H1|T],X).
max_score([_|T],X):-
	max_score(T,X).
 	
%%	replace_nth(+Index, +List, +Element, -NewList) is det.
%
%	Replace the Nth (1-based) element of a list.

replace_nth(1, [_|T], V, [V|T]) :- !.
replace_nth(I, [H|T0], V, [H|T]) :-
	I2 is I - 1,
	replace_nth(I2, T0, V, T).
